require("dotenv").config()

const faunadb = require("faunadb")

const q = faunadb.query
var clientAdmin = new faunadb.Client({ secret: process.env.FAUNA_ADMIN_KEY })
module.exports = {
  q,
  clientAdmin
}
