Playground based on an altered version of https://fauna.com/tutorials/social
(Purpose partially being to check what is best practice with fauna, don't consider this as best practice.)

---

To start you need to:
1. create a database on **Fauna**
2. add your **Admin Credential** in the _.env.sample_ file (which you need to change to **.env**)
3. open the **run.js** and follow the step-by-step commands guide or fool around

Have fun.

