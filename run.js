

const args = process.argv.slice(2)
const arg1 = args[0]
const arg2 = args[1]
const arg3 = args[2]
const args4plus = args.slice(3)

const asyncRun = require("./asyncRun") // utility function
const { clientAdmin, q } = require("./client")

/**
 * Functional pieces of code
 */
const getRef = object => q.Select("ref", object)
const matchPeopleByName = name => q.Match(q.Index("people_by_name"), name)
const getPeopleByName = name => q.Get(matchPeopleByName(name))
const matchFollowersByFollowee = followee => q.Match(q.Index("people_by_followee"), followee)
const getFollowersByFollowee = followee => q.Get(matchFollowersByFollowee(followee))
// const matchPeopleByFollowee = followee => q.Match(q.Index("people_by_followee"), followee)
// const matchFollowersByFollowee = name => q.Match(q.Index("followers_by_followee"), name)

let query, thenableQuery

/**
 * INSTRUCTIONS
 * -----------------
 * Steps to run with : node run [arg1] [arg2]
 * -----------------
 * 1. createClass people
 * 2. createIndex people_by_name
 * 3. createIndex people_by_followee
 * 5. createIndex ...
 * 6. createInstances people
 * 7. getPerson alice (or whoever else you want)
 * 8. alice follows bob
 * 9. alice follows dave
 * 10. carol follows bob
 * 11. dave follows carol
 * 12. getFollowers bob
 * 13. getFollowee bob
 * 14. getAllFollowers bob dave
 * 15. getAllFollowee alice carol dave
 */

if (arg1 === "createClass" && arg2) query = q.CreateClass({ name: arg2 })
if (arg1 === "createIndex") {
  if (arg2 === "people_by_name") {
    query = q.CreateIndex({
      name: arg2,
      source: q.Class("people"),
      terms: [{ field: ["data", "name"] }],
      unique: true
    })
  }
  if (arg2 === "people_by_followee") {
    query = q.CreateIndex({
      name: arg2,
      source: q.Class("people"),
      terms: [{ field: ["data", "followee"] }]
    })
  }
}
if (arg1 === "createInstances") {
  if (arg2 === "people") {
    query = q.Foreach(["alice", "bob", "carol", "dave"], function(name) {
      return q.Create(q.Class("people"), { data: { name: name, followee: [] } })
    })
  }
}
if (arg1 === "getPerson" && arg2) {
  query = getPeopleByName(arg2)
}
if (arg2 === "follows" && arg1 && arg3) {
  thenableQuery = clientAdmin
    .query(
      q.Let(
        {
          followee: q.Select(["data", "followee"], getPeopleByName(arg1)) || [],
          newFollowee: q.Select("ref", getPeopleByName(arg3))
        },
        () => [q.Var("followee"), q.Var("newFollowee")]
      )
    )
    .then(([followee, newFollowee]) => {
      if (followee.map(f => f.toString()).includes(newFollowee.toString())) throw Error("Already following that person!")
      return clientAdmin.query(
        q.Update(getRef(getPeopleByName(arg1)), {
          data: {
            followee: [...followee, newFollowee]
          }
        })
      )
    })
}
if (arg1 === "getFollowers" && arg2) {
  query = q.Select(
    "data",
    q.Map(q.Paginate(matchFollowersByFollowee(getRef(getPeopleByName(arg2)))), follower => q.Select(["data", "name"], q.Get(follower)))
  )
}
if (arg1 === "getAllFollowers" && arg2) {
  let matchedPersonList = [arg2, arg3, ...args4plus].filter(arg => arg !== undefined)
  query = q.Select(
    "data",
    q.Map(q.Paginate(q.Union(...matchedPersonList.map(arg => matchFollowersByFollowee(getRef(getPeopleByName(arg)))))), follower =>
      q.Select(["data", "name"], q.Get(follower))
    )
  )
}
if (arg1 === "getFollowee" && arg2) {
  query = q.Map(q.Select(["data", "followee"], getPeopleByName(arg2)), followee => q.Select(["data", "name"], q.Get(followee)))
}
if (arg1 === "getAllFollowee" && arg2) {
  let matchedPersonList = [arg2, arg3, ...args4plus].filter(arg => arg !== undefined)

  thenableQuery = clientAdmin
    .query(
      matchedPersonList.map(arg =>
        q.Map(q.Select(["data", "followee"], getPeopleByName(arg)), followee => q.Select(["data", "name"], q.Get(followee)))
      )
    )
    .then(items => items.reduce((list, item) => list.push(...item) && list, []))
    .then(items => Array.from(new Set(items)))
}

// Actually run the query
if (query || thenableQuery) {
  ;(async () => {
    if (thenableQuery) await asyncRun(thenableQuery)
    else await asyncRun(clientAdmin.query(query))
  })()
} else {
  console.log("You may have a typo ;) (no command found)")
}
