async function asyncRun(prom) {
  let r = await new Promise((res) => res(prom)).catch(console.error)
  console.log(JSON.stringify(r, null, 2))
}

module.exports = asyncRun